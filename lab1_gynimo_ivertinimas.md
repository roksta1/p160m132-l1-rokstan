Studentas: Rokas Staniulis

Akademinė grupė: MGTMM-5

| Dalis     | Įvertinimas         | Paaiškinimas
| ----------| ------------------- | ---------------------------------------------------------------
| P 1-1     |        1.00 /  1.00 | funkcijos sukūrimas
| P 1-2     |        0.80 /  1.00 | filtravimas panaudojant funkciją iš P 1-1
| P 1-3     |        1.00 /  1.00 | filtravimas panaudojant anoniminę funkciją
| P 2-1     |        1.00 /  1.00 | žodžių filtravimas
| P 2-2     |        1.00 /  1.00 | žodžių dažnio skaičiavimas
| P 2-3     |        1.00 /  1.00 | žodžių dažnio rikiavimas mažėjimo tvarka
| P 3-1     |        1.00 /  1.00 | įrašų filtravimas
| P 3-2     |        1.00 /  1.00 | kintamojo reikšmių išrinkimas
| P 3-3     |        1.00 /  1.00 | vidurkio ir standartinio nuokrypio skaičiavimas
| P 3-4     |        1.00 /  1.00 | sumavimas grupuojant pagal vardų skalės kintamajo reikšmes
| **Lab1-P**|        9.80 / 10.00 | _Apache Spark_ praktinės dalies pažymys
| **Lab1-T**|        6.00 / 10.00 | testo pažymys
| **Lab1**  |        9.00 / 10.00 | bendras laboratorinio darbo gynimo pažymys
